#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include < opencv2\stitching\stitcher.hpp >  
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>


using namespace cv;
using namespace std;

double stdev = 2;
int blur_dim = 7;
int lower_threshold = 25;
int upper_threshold = 25;
int min_radius = 15;
int max_radius = 30;

int main(int argc, char** argv){
	string filename;
	string filetype = ".jpg";
	

	float avg_rad = 0;
	int num = 0;

	float x = 0;
	float y = 0;
	float radius = 0; 
	
	vector<Mat> images;
	vector<Mat> gauss_images;
	vector<vector<Vec3f>> hough_images;

	vector<Mat> result;

	for (int i = 1; i < 101; i++){

		filename = "res/5um-40x-3-" + to_string(i) + filetype;
		Mat tmp = imread(filename, IMREAD_GRAYSCALE);
		if (tmp.empty()) {
			cout << filename;
			break;
		}

		images.push_back(tmp);

		Mat gaussian;
		vector<Vec3f> circles;

		GaussianBlur(tmp, gaussian, Size(blur_dim, blur_dim), stdev, stdev);
		HoughCircles(gaussian, circles, CV_HOUGH_GRADIENT, 1, 1, upper_threshold, lower_threshold, min_radius, max_radius);
		
		gauss_images.push_back(gaussian);
		hough_images.push_back(circles);
		if (circles.size() != 0){

			for (int j = 0; j < circles.size(); j++){
				radius += circles[j][2];
			}
			radius = cvRound(radius / circles.size());
			avg_rad += radius;
			num++;
		}

	}

	avg_rad = cvRound(avg_rad/ num);

	printf("Float %f", avg_rad);
	for (int i = 0; i < images.size(); i++){
		Mat img = images.at(i);
		Mat gauss = gauss_images.at(i);
		vector<Vec3f> circles = hough_images.at(i);

		Mat black = Mat::zeros(img.rows, img.cols, CV_8U);

		if (circles.size() != 0){

			x = 0;
			y = 0;

			for (int j = 0; j < circles.size(); j++){
				x += circles[j][0];
				y += circles[j][1];
			}

			x = cvRound(x / circles.size());
			y = cvRound(y / circles.size());
		}

		Point center(x, y);
		Scalar white(255, 255, 255);

		rectangle(black, Point(x - avg_rad, y - avg_rad), Point(x + avg_rad, y + avg_rad), white, -1);
		bitwise_and(img, black, img);

		Mat input = img(Rect(x - avg_rad, y - avg_rad, avg_rad * 2, avg_rad * 2)).clone();

		int len = std::max(input.cols, input.rows);
		Point2f pt(len / 2., len / 2.);
		Mat r = getRotationMatrix2D(pt, 40, 1.0);

		warpAffine(input, input, r, Size(len, len));

		avg_rad *= 3 / 4;
		cvtColor(input, input, CV_GRAY2BGR);
		imwrite("outputs/" + to_string(i) + ".jpg", input);

	}

	return 0;

}







